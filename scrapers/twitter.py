import json
import os
import tweepy

from .base_scraper import BaseScraper
file_dir = os.path.dirname(os.path.abspath(__file__))


class Twitter(BaseScraper):
	"""A simple class to query twitter"""

	def __init__(self, credentials_file=None):
		if credentials_file is None:
			self.credentials_file = file_dir + '/../credentials/twitter_credentials.json'
		else:
			self.credentials_file = credentials_file

		self.set_twitter_api()
		self.language = "en"

	def __str__(self):
		return 'This is the Twitter class!'

	def set_language(self, language):
		self.language = language

	def set_twitter_api(self):
		with open(self.credentials_file) as cred_data:
			info = json.load(cred_data)
			consumer_key = info['CONSUMER_KEY']
			consumer_secret = info['CONSUMER_SECRET']

		auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
		self.api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

	def get_list_of_data(self, hashtag, limit=10):
		tweet_list = list()

		for tweet in tweepy.Cursor(self.api.search, q='#' + hashtag, rpp=100, lang=self.language).items(limit):
			# t = str(tweet.text.encode('utf-8'))
			tweet_list.append(str(tweet.text))

		return tweet_list

	def get_clean_words(self):
		return ['re']

