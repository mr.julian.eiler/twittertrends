import abc


class BaseScraper(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def __str__(self):
        raise NotImplementedError('users must define __str__ to use this base class')

    @abc.abstractmethod
    def __init__(self):
        raise NotImplementedError('users must define __init__ to use this base class')

    @abc.abstractmethod
    def get_list_of_data(self):
        raise NotImplementedError('users must define get_list_of_data to use this base class')

    @abc.abstractmethod
    def get_clean_words(self):
        raise NotImplementedError('users must define get_list_of_data to use this base class')