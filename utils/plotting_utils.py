from wordcloud import WordCloud
import matplotlib.pyplot as plt
import numpy as np
from wordcloud import STOPWORDS

def plot_wordcloud(tweets, filename=None):
	wordcloud = WordCloud(max_font_size=50, max_words=50, background_color="white").generate(tweets)
	plt.figure()
	plt.imshow(wordcloud, interpolation="bilinear")
	plt.axis("off")
	plt.show()
	if filename is not None:
		wordcloud.to_file(filename)
