import re

def clean_re(line):
	line = re.sub(r'[.,"!]+', '', line, flags=re.MULTILINE)  # removes the characters specified
	line = re.sub(r'^RT[\s]+', '', line, flags=re.MULTILINE)  # removes RT
	line = re.sub(r'https?:\/\/.*[\r\n]*', '', line, flags=re.MULTILINE)  # remove link
	line = re.sub(r'[:]+', '', line, flags=re.MULTILINE)
	import string
	printable = set(string.printable)
	line = ''.join(filter(lambda x: x in string.printable, line))  # filter non-ascii characers


	new_line = ''
	for i in line.split():  # remove @ and #words, punctuataion
		if not i.startswith('@') and not i.startswith('#') and i not in string.punctuation:
			new_line += i + ' '
	line = new_line

	return line


def remove_words(line, r_words=[]):
	querywords = line.split()

	resultwords = [word for word in querywords if word.lower() not in r_words]
	result = ' '.join(resultwords)

	return result


def clean_list_re_and_remove(text_list, r_words=[]):
	print('text lost: [{}]'.format(text_list))

	cleaned_list = list()
	for line in text_list:
		line = line.lower()

		cleaned = clean_re(line)
		r_cleaned = remove_words(cleaned, r_words=r_words)
		cleaned_list.append(r_cleaned)

	print('cleaned_list: [{}]'.format(cleaned_list))

	return cleaned_list
