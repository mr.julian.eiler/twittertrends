# TwitterTrends

## Steps
0. Pre-requisites:
    - Python 3 
    - Pip
    - virtualenvwrapper (just a recommendation)
1. Install requirements  
`pip install requirements.txt`
2. Run script  
`python tests/test_twitter.py search-term`