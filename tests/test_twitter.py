import sys
import os

file_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(file_dir + '/..')

from scrapers.twitter import Twitter

from utils.clean import clean_list_re_and_remove
from utils.plotting_utils import plot_wordcloud


def main(argv):
    query = argv[0]
    twitter = Twitter()
    tweets = twitter.get_list_of_data(query, limit=int(100))
    print(tweets)
    r_words = [query, 'rt', 'https']
    r_words += twitter.get_clean_words()
    cleaned_posts = clean_list_re_and_remove(tweets, r_words=r_words)

    words = ""
    for post in cleaned_posts:
        words += post

    plot_wordcloud(words)


if __name__ == "__main__":
    main(sys.argv[1:])
